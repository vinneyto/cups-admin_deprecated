# MiuCups admin ui

### Development

Install dependencies

    $ yarn install

Create `.env` file and put dev server config to it.

For example
```
PORT=3334
SERVER_PORT=3337
```

Then start development server

    $ yarn start

### Production

Build project with command

    $ yarn run build

All necessary static files will be moved to `dist` directory

