
module.exports = angular.module('cups.directives', [
    require('./submit-valid').name,
    require('./no-click-through').name,
    require('./st-ready').name,
    require('./table-sort').name,
    require('./loading').name
]);
