'use strict';

module.exports = angular.module('cups.directives.noClickThrough', [])

    /**
     * @ngdoc directive
     * @name noClickThrough
     *
     * Директива запрещает щелчек через документ
     */
    .directive('noClickThrough', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind('click', function(event) {
                    event.stopPropagation();
                });
            }
        };
    });
