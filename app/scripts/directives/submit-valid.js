'use strict';

module.exports = angular.module('cups.directives.submitValid', [])
    .directive('submitValid', function() {
        return {
            require: '^form',
            link: function(scope, formOrButton, attributes, formCtrl) {
                var event = formOrButton.is('form') ? 'submit' : 'click';
                formOrButton.bind(event, function() {
                    scope.$apply(function() {
                        var action = attributes.submitValid;
                        if (formCtrl.$valid) {
                            scope.$eval(action);
                        } else {
                            var form = formOrButton.is('form') ? formOrButton : formOrButton.parents('form');
                            $('.ng-invalid', form).first().focus();
                        }
                    });
                });
            }
        };
    });
