'use strict';

module.exports = angular.module('cups.directives.stReady', [])

    .directive('stReady', function($parse) {
        return {
            restrict: 'A',
            require: '^stTable',
            link: function(scope, element, attr, stCtrl) {
                $parse(attr['stReady'])(scope, {$table: stCtrl});
            }
        };
    });
