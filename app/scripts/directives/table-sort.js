'use strict';

module.exports = angular.module('cups.directives.tableSort', [])

    .directive('tableSort', function($state) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                let field = attr['tableSort'];

                scope.$watch(() => $state.params, () => {
                    element.toggleClass('st-sort-ascent',  $state.params._sort === field && !$state.params._reverse);
                    element.toggleClass('st-sort-descent', $state.params._sort === field &&  $state.params._reverse);
                }, true);

                element.on('click', () => {
                    scope.$apply(() => {
                        $state.go('.', {
                            _sort: field,
                            _reverse: $state.params._sort === field ? !$state.params._reverse : false
                        });
                    });
                });
            }
        };
    });
