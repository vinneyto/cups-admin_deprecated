
module.exports = angular.module('cups.directives.loading', [])

    .directive('loading', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {

                let timeout = null;

                scope.$watch(attr['loading'], (loading) => {
                    if (loading) {
                        timeout = $timeout(() => {
                            showLoader();
                            timeout = null;
                        }, 200);
                    } else {
                        if (timeout) {
                            $timeout.cancel(timeout);
                        }
                        hideLoader();
                        timeout = null;
                    }
                });

                function showLoader() {
                    if (!attr['ngDisabled']) { element.attr('disabled', 'disabled'); }
                    element.append('<span class="fa fa-spinner rotating"></span>');
                }

                function hideLoader() {
                    if (!attr['ngDisabled']) { element.attr('disabled', null); }
                    element.find('.fa-spinner').remove();
                }
            }
        }
    });
