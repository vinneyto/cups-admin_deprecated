
module.exports = angular.module('cups.models.session', [
    require('./storage').name
])

    .factory('Session', function (Storage) {
        const Resource = Storage.defineResource({
            idAttribute: 'token',
            name: 'session',
            endpoint: 'session'
        });

        return Resource;
    });
