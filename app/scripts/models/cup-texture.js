
module.exports = angular.module('cups.models.cupTexture', [
    require('./storage').name
])

    .factory('CupTexture', function ($q, DSHttpAdapter, Storage, Upload) {

        class CupTexture {

            upload() {
                if (this.$file) {
                    return Upload.upload({
                        url: `${DSHttpAdapter.defaults.basePath}/cup/texture/`,
                        data: {file: this.$file},
                        headers: angular.copy(DSHttpAdapter.defaults.httpConfig.headers)
                    }).then(response => Resource.inject(response.data));
                }
                return $q.when(this);
            }
        }

        const Resource = Storage.defineResource({
            name: 'cupTexture',
            endpoint: 'cup/texture',
            useClass: CupTexture
        });

        return Resource;
    });
