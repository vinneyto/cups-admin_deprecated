
module.exports = angular.module('cups.models.galleryImageFile', [
    require('./storage').name
])

    .factory('GalleryImageFile', function ($q, DSHttpAdapter, Storage, Upload) {

        class GalleryImageFile {

            upload() {
                if (this.$file) {
                    return Upload.upload({
                        url: `${DSHttpAdapter.defaults.basePath}/gallery/image/`,
                        data: {file: this.$file},
                        headers: angular.copy(DSHttpAdapter.defaults.httpConfig.headers)
                    }).then(response => Resource.inject(response.data));
                }
                return $q.when(this);
            }
        }

        const Resource = Storage.defineResource({
            name: 'galleryImageFile',
            endpoint: 'gallery/image',
            useClass: GalleryImageFile
        });

        return Resource;
    });
