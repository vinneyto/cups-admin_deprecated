
module.exports = angular.module('cups.models.cup', [
    require('./storage').name,
    require('./cup-texture').name
])

    .factory('Cup', function (Storage, DSHttpAdapter, CupTexture) {

        class Cup {

            setPriority(priority) {
                return DSHttpAdapter.HTTP({
                    method: 'PUT',
                    url: `/cup/${this.id}/priority/${priority}`,
                    verbsUseBasePath: true
                });
            }

            addOnLanding(position) {
                return DSHttpAdapter.HTTP({
                    method: 'PUT',
                    url: `/cup/${this.id}/landing/${position}`,
                    verbsUseBasePath: true
                }).then(response => {
                    _.assign(this, _.pick(response.data, ['show_on_landing', 'priority']));
                });
            }

            removeFromLanding() {
                return DSHttpAdapter.HTTP({
                    method: 'DELETE',
                    url: `/cup/${this.id}/landing`,
                    verbsUseBasePath: true
                }).then(response => {
                    _.assign(this, _.pick(response.data, ['show_on_landing', 'priority']));
                });
            }

            clone() {
                return Storage.createInstance('cup', DSHttpAdapter.defaults.serialize(null, this));
            }
        }

        const Resource = Storage.defineResource({
            name: 'cup',
            endpoint: 'cup',
            useClass: Cup,
            relations: {
                belongsTo: {
                    [CupTexture.name]: {
                        localField: 'texture',
                        localKey: 'texture_id'
                    }
                }
            }
        });

        return Resource;
    });
