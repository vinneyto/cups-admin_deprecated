
module.exports = angular.module('cups.models.order', [
    require('./storage').name,
    require('./attachment').name
])

    .factory('Order', function (DSHttpAdapter, Storage, Attachment) {

        class Order {
            clone() {
                return Storage.createInstance('order', DSHttpAdapter.defaults.serialize(null, this));
            }
        }

        const Resource = Storage.defineResource({
            name: 'order',
            endpoint: 'order',
            useClass: Order,
            relations: {
                hasMany: {
                    [Attachment.name]: {
                        localKeys: 'attachment_ids',
                        localField: 'attachments'
                    }
                }
            }
        });

        return Resource;
    });
