
module.exports = angular.module('cups.models.storage', [
    require('../services/utils').name
])

    .factory('Storage', function (DS, DSHttpAdapter, authStorage, Utils) {

        DSHttpAdapter.defaults.basePath = '/api';
        DSHttpAdapter.defaults.forceTrailingSlash = true;
        DSHttpAdapter.defaults.deserialize = function(resource, response) {
            resource.lastMeta = {};
            if (response.headers('x-total-count')) {
                resource.lastMeta.totalCount = parseInt(response.headers('x-total-count'));
            }
            return response.data;
        };

        DSHttpAdapter.defaults.serialize = function(resourceConfig, data)  {
            return Utils.serialize(data);
        };

        DSHttpAdapter.defaults.httpConfig = {
            ignoreLoadingBar: true,
            headers: {
                'Authorization': ()=> {
                    let session = authStorage.getSession();
                    return session && `JWT ${session.token}`;
                }
            }
        };

        DS.defaults.beforeUpdate = function(resource, data, cb) {
            if (resource.partial) {
                resource.method = 'PATCH';
            }
            cb(null, data);
        };

        return DS;
    });
