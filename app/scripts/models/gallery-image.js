
module.exports = angular.module('cups.models.galleryImage', [
    require('./storage').name,
    require('./gallery-image-file').name
])

    .factory('GalleryImage', function (DSHttpAdapter, Storage, GalleryImageFile) {

        class GalleryImage {
            clone() {
                return Storage.createInstance('galleryImage', DSHttpAdapter.defaults.serialize(null, this));
            }
        }

        const Resource = Storage.defineResource({
            name: 'galleryImage',
            endpoint: 'gallery',
            useClass: GalleryImage,
            relations: {
                belongsTo: {
                    [GalleryImageFile.name]: {
                        localField: 'image_file',
                        localKey: 'image_file_id'
                    }
                }
            }
        });

        return Resource;
    });
