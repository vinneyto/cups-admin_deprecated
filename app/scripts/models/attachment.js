
module.exports = angular.module('cups.models.attachment', [
    require('./storage').name
])

    .factory('Attachment', function ($q, DSHttpAdapter, Upload, Storage) {

        class Attachment {
            upload() {
                if (this.$file) {
                    return Upload.upload({
                        url: `${DSHttpAdapter.defaults.basePath}/core/attachment/`,
                        data: {file: this.$file},
                        headers: angular.copy(DSHttpAdapter.defaults.httpConfig.headers)
                    }).then(response => Resource.inject(response.data));
                }
                return $q.when(this);
            }
        }

        const Resource = Storage.defineResource({
            name: 'attachment',
            endpoint: 'core/attachment',
            useClass: Attachment
        });

        return Resource;
    });
