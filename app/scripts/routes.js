

module.exports = angular.module('cups.routes', [
    require('./services/auth-storage').name,
    require('./services/app-state').name,

    require('../components/public-layout/public-layout').name,
    require('../components/login-page/login-page').name,
    require('../components/layout/layout').name,

    require('../components/cup-list/cup-list').name,
    require('../components/cup-details/cup-details').name,

    require('../components/gallery-image-list/gallery-image-list').name,
    require('../components/gallery-image-details/gallery-image-details').name,

    require('../components/order-list/order-list').name,
    require('../components/order-details/order-details').name
])

    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        $locationProvider.html5Mode(true);

        $urlRouterProvider
            .otherwise(function($injector, $location) {
                const $state = $injector.get('$state'),
                      authStorage = $injector.get('authStorage');

                if ($location.path() === '' || $location.path() === '/') {
                    if (authStorage.getSession()) {
                        $state.go('private.orders');
                    } else {
                        $state.go('public.login');
                    }
                } else {
                    $state.go('public.404', {}, {location: false});
                }

                return $location.path() || '/';
            });

        let listController = ['$scope', 'records', ($scope, records) => $scope.records = records];
        let detailsController = ['$scope', 'record', ($scope, record) => $scope.record = record];

        $stateProvider
            .state('public', {
                abstract    : true,
                template    : '<public-layout><ui-view></ui-view></public-layout>'
            })
            .state('public.404', {
                url         : '/404',
                template    : '<h1 class="text-center m-t-lg">Страница не найдена</h1>'
            })
            .state('public.login', {
                url         : '/login',
                template    : '<login-page></login-page>'
            })
        ;

        $stateProvider
            .state('private', {
                abstract    : true,
                template    : '<layout><ui-view></ui-view></layout>',
                resolve     : {
                    checkSession: ($q, authStorage, AppState) => {
                        let checkSessionPromise = authStorage.getSession() ? $q.when() : $q.reject('session not found');

                        return checkSessionPromise.then(() => AppState.refreshNewOrdersAmount());
                    }
                }
            })
            .state('private.cups', {
                url         : '/cups?' + [
                    '{_sort:string}',
                    '{_reverse:bool}',
                    '{_offset:int}',
                    '{_limit:int}',
                    '{name:string}',
                    '{show_on_landing:bool}'
                ].join('&'),
                template    : '<cup-list records="records"></cup-list>',
                resolve     : {
                    records: (checkSession, cupListLoad, $stateParams) => cupListLoad($stateParams)
                },
                controller  : listController,
                params      : {
                    _sort: 'name',
                    _reverse: false,
                    _offset: 0,
                    _limit: 10
                }
            })
            .state('private.cup-details', {
                url: '/cups/:id',
                template: '<cup-details record="record"></cup-details>',
                resolve     : {
                    record: (checkSession, loadCupById, $stateParams) => loadCupById($stateParams.id)
                },
                controller  : detailsController,
            })
            .state('private.gallery', {
                url         : '/gallery?' + [
                    '{_sort:string}',
                    '{_reverse:bool}',
                    '{_offset:int}',
                    '{_limit:int}',
                    '{name:string}',
                    '{created__gte:date}',
                    '{created__lte:date}',
                ].join('&'),
                template    : '<gallery-image-list records="records"></gallery-image-list>',
                resolve     : {
                    records: (checkSession, galleryImageListLoad, $stateParams) => galleryImageListLoad($stateParams)
                },
                controller  : listController,
                params      : {
                    _sort: 'created',
                    _reverse: true,
                    _offset: 0,
                    _limit: 20
                }
            })
            .state('private.gallery-image-details', {
                url: '/gallery/:id',
                template: '<gallery-image-details record="record"></gallery-image-details>',
                resolve     : {
                    record: (checkSession, loadGalleryImageById, $stateParams) => loadGalleryImageById($stateParams.id)
                },
                controller  : detailsController,
            })
            .state('private.orders', {
                url         : '/orders?' + [
                    '{_sort:string}',
                    '{_reverse:bool}',
                    '{_offset:int}',
                    '{_limit:int}',
                    '{fullname:string}',
                    '{phone:string}',
                    '{email:string}',
                    '{opened:bool}',
                    '{created__gte:date}',
                    '{created__lte:date}',
                ].join('&'),
                template    : '<order-list records="records"></order-list>',
                resolve     : {
                    refreshNewOrdersAmount: (checkSession, AppState) => AppState.refreshNewOrdersAmount(),
                    records: (checkSession, orderListLoad, $stateParams) => orderListLoad($stateParams)
                },
                controller  : listController,
                params      : {
                    _sort: 'created',
                    _reverse: true,
                    _offset: 0,
                    _limit: 20
                }
            })
            .state('private.order-details', {
                url: '/order/:id',
                template: '<order-details record="record"></order-details>',
                resolve     : {
                    record: (checkSession, loadOrderById, $stateParams) => loadOrderById($stateParams.id)
                },
                controller  : detailsController,
            })
        ;

        $httpProvider.interceptors.push(function($q, $location, authStorage) {
            return {
                'responseError': function(response) {
                    if(response.status === 401) {
                        authStorage.clearSession();
                        $location.path('/');
                    }
                    return $q.reject(response);
                }
            };
        });
    })

    .run(function($rootScope, $state) {
        // Перехват ошибок во время смены состояния. Вывод в консоль и переход на страницу логина.
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            console.warn('Transition aborted:', JSON.stringify(error));
            if (error && error.status === 404) {
                $state.go('public.404');
            } else if (toState.name !== 'public.login') {
                $state.go('public.login');
            }
        });

        $rootScope.$on('$stateChangeSuccess', function () {
            $('.app-body').scrollTop(0);
        });
    });
