
angular.module('cups').config(function($animateProvider) {
    $animateProvider.classNameFilter(/^(?:(?!ui-select-container|rotating).)*$/)
});
