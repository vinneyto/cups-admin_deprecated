
angular.module('cups').config(function($httpProvider) {

    $httpProvider.interceptors.push(['$q', '$injector', ($q, $injector) => {
        return {
            'responseError': function(response) {
                $injector.get('Notification').error({
                    message: `${response.config.method} ${response.config.url} 
                              <br> ${response.status} 
                              <br> ${(response.data && response.data.detail) || 'Неизвестная ошибка'}`
                });

                return $q.reject(response);
            }
        }
    }])
});
