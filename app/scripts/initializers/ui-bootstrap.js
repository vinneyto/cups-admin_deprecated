
const datepickerDayTemplate = require('!html-loader!../../templates/datepicker/day.html');
const datepickerMonthTemplate = require('!html-loader!../../templates/datepicker/month.html');
const datepickerYearTemplate = require('!html-loader!../../templates/datepicker/year.html');

angular.module('cups').run(($templateCache) => {
    $templateCache.put('uib/template/datepicker/day.html', datepickerDayTemplate);
    $templateCache.put('uib/template/datepicker/month.html', datepickerMonthTemplate);
    $templateCache.put('uib/template/datepicker/year.html', datepickerYearTemplate);
});
