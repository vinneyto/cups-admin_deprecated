

module.exports = angular.module('cups.services.authStorage', [])
    .service('authStorage', function() {
        var KEY = 'cups_session';

        class authStorage {
            static currentSession = null;

            /**
             * Сохраняет данные аутентификации в localStorage.
             */
            static setSession(session) {
                this.clearSession();
                localStorage.setItem(KEY, JSON.stringify(session));
                this.currentSession = session;
            }

            /**
             * Возвращает текущую сессию.
             */
            static getSession() {
                if (this.currentSession) return this.currentSession;

                var serialized = localStorage.getItem(KEY);
                if (!serialized) return null;

                this.currentSession = JSON.parse(serialized);
                return this.currentSession;
            }

            /**
             * Очищает данные сессии из localStorage.
             */
            static clearSession() {
                localStorage.removeItem(KEY);
                this.currentSession = null;
            }
        }

        return authStorage;
    });
