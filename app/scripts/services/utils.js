
module.exports = angular.module('cups.services.utils', [])

    .factory('Utils', function () {

        /**
         * Копирует объект и подобъекты, исключает все свойства имена которых начинаются с excludePrefix
         * @param record
         * @param excludePrefix
         * @returns {*}
         */
        function serialize(record, excludePrefix = '$') {
            if (_.isArray(record)) {
                return _.map(record, item => serialize(item, excludePrefix))
            } else
            if (_.isObject(record) && !_.isDate(record) && !_.isFunction(record)) {
                let data = {};
                for (let key in record) {
                    if (record.hasOwnProperty(key) && !_.startsWith(key, excludePrefix)) {
                        data[key] = serialize(record[key], excludePrefix);
                    }
                }
                return data;
            } else {
                return record;
            }
        }

        return { serialize };
    });
