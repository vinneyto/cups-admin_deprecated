
module.exports = angular.module('cups.services.appState', [
    require('../../scripts/models/order').name
])

    .factory('AppState', function (Order) {

        let newOrdersAmount = 0;

        function getNewOrdersAmount() {
            return newOrdersAmount;
        }

        function refreshNewOrdersAmount() {
            return Order.findAll({_offset: 0, _limit: 1, opened: false}, {bypassCache: true}).then(() => {
                newOrdersAmount = Order.lastMeta.totalCount;
            });
        }

        return {
            getNewOrdersAmount,
            refreshNewOrdersAmount
        };
    });
