
module.exports = angular.module('cups.models.withLoadingBar', [])

    .factory('withLoadingBar', function (cfpLoadingBar) {

        return function (promise) {
            cfpLoadingBar.start();

            return promise.finally(() => {
                cfpLoadingBar.complete();
            });
        }
    });
