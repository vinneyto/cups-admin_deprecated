
module.exports = angular.module('cups.filters.dateFormat', [])

    .filter('dateFormat', function ($filter) {
        return function (input) {
            return $filter('date')(input, 'dd-MM-yyyy HH:mm')
        }
    });
