
module.exports = angular.module('cups.filters', [
    require('./cup-link').name,
    require('./date-format').name
]);
