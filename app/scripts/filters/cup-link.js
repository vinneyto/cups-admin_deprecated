
module.exports = angular.module('cups.filters.cupLink', [])

    .filter('cupLink', function () {
        return function (input) {
            return 'https://cups.vinneyto.org/cup/' + input;
        }
    });
