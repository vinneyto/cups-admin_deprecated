
require('./table-default.html');
require('./table-on-landing.html');
const BaseTable = require('../base-table/base-table').BaseTable;


class Controller extends BaseTable {

    filters = {
        show_on_landing: {
            options: [
                { name: 'Да', value: true },
                { name: 'Нет', value: false }
            ],
            value: null
        },
        name: {
            value: null
        }
    };

    sortableOptions = {
        axis: 'y',
        items: 'tbody > tr',
        helper: function(e, ui) {
            ui.parent().children().each(function () {
                $(this).children().each(function() {
                    $(this).width($(this).width());
                });
            });
            return ui;
        }
    };

    constructor($state, Cup, CupAddModal) {
        super($state);
        this.Cup = Cup;
        this.CupAddModal = CupAddModal;
        this.onLandingMode = $state.params.show_on_landing && $state.params._sort === 'priority';
        this.restoreFilters();
    }

    serializeFilters() {
        let params = super.serializeFilters();

        if (params.show_on_landing) {
            params._limit = 200;
            params._sort = 'priority';
        }
        return params;
    }

    addCup() {
        this.CupAddModal.open()
    }

    onUpdateOrder = (e, ui) => {
        let {index, dropindex} = ui.item.sortable;
        let sourceItem = this.records[index];
        let targetItem = this.records[dropindex];

        sourceItem.setPriority(targetItem.priority);
    }
}

module.exports = angular.module('cups.components.cupList', [
    require('../../scripts/models/cup').name,
    require('../cup-add-modal/cup-add-modal').name
])

    .factory('cupListLoad', function (Cup, withLoadingBar) {
        return function (params) {
            let query = {
                _ordering: (params._reverse ? '-' : '') + params._sort,
                _offset: params._offset,
                _limit: params._limit
            };
            if (!_.isNil(params.show_on_landing)) {
                query.show_on_landing = params.show_on_landing;
            }
            if (params.name) {
                query.name__icontains = params.name;
            }
            return withLoadingBar(Cup.findAll(query, {bypassCache: true}))
        }
    })

    .component('cupList', {
        bindings: {
            records: '='
        },
        templateUrl: require('./cup-list.html'),
        controller: Controller
    });
