
const templateUrl = require('./order-add-modal.html');

class Controller {
    attachments = [];

    constructor($q, $uibModalInstance, $state, Order, Notification) {
        this.$q = $q;
        this.$uibModalInstance = $uibModalInstance;
        this.Notification = Notification;
        this.$state = $state;
        this.Order = Order;

        this.record = Order.createInstance();
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }

    save() {
        this.saving = true;
        let { $q, record, attachments } = this;

        $q.all(_.map(attachments, a => a.upload()))
            .then(attachments => { record.attachment_ids = _.map(attachments, 'id'); return record; })
            .then(record => record.DSCreate())
            .then(record => this.$state.go('private.order-details', {id: record.id}))
            .then(() => this.Notification.success({message: 'Заказ сохранен', delay: 3000}))
            .then(() => this.cancel())
            .finally(() => this.saving = false);
    }
}

module.exports = angular.module('cups.components.orderAddModal', [
    require('../../scripts/models/order').name
])

    .factory('OrderAddModal', ($uibModal) => ({
        open: function () {
            return $uibModal.open({
                controller: Controller,
                controllerAs: '$ctrl',
                templateUrl,
                size: 'lg'
            }).result;
        }
    }));
