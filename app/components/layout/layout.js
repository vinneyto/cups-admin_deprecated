
require('./layout.scss');

class Controller {
    isCollapsed = true;

    constructor($rootScope, $state, authStorage, CupAddModal, GalleryImageAddModal, OrderAddModal, AppState) {
        this.$state = $state;
        this.authStorage = authStorage;
        this.CupAddModal = CupAddModal;
        this.GalleryImageAddModal = GalleryImageAddModal;
        this.OrderAddModal = OrderAddModal;
        this.AppState = AppState;

        $rootScope.$on('$stateChangeSuccess', () => {
            this.isCollapsed = true;
        });
    }

    getUserName() {
        let session = this.authStorage.getSession();
        return session && (session.user.email || session.user.username);
    }

    logout() {
        this.authStorage.clearSession();
        this.$state.go('public.login');
    }

    addCup() {
        this.CupAddModal.open();
    }

    addImage() {
        this.GalleryImageAddModal.open();
    }

    addOrder() {
        this.OrderAddModal.open();
    }
}

module.exports = angular.module('cups.components.layout', [
    require('../../scripts/services/auth-storage').name,
    require('../../scripts/services/app-state').name,
    require('../cup-add-modal/cup-add-modal').name,
    require('../gallery-image-add-modal/gallery-image-add-modal').name,
    require('../order-add-modal/order-add-modal').name
])

    .component('layout', {
        transclude: true,
        templateUrl: require('./layout.html'),
        controller: Controller
    });
