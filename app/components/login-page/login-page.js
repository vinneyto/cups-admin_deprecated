
import './login-page.scss';

class Controller {
    username = null;
    password = null;

    constructor($state, authStorage, Session) {
        this.$state = $state;
        this.authStorage = authStorage;
        this.Session = Session;
    }

    login() {
        let { $state, authStorage, Session, username, password } = this;

        this.loading = true;

        Session.create({username, password}).then(session => {
            authStorage.setSession(session);
            $state.go('private.orders');
        }).finally(() => {
            this.loading = false;
        });
    }
}

module.exports = angular.module('cups.components.loginPage', [
    require('../../scripts/models/session').name,
    require('../../scripts/services/auth-storage').name
])

    .component('loginPage', {
        templateUrl: require('./login-page.html'),
        controller: Controller
    });

