
require('./loading-overlay.scss');

module.exports = angular.module('cups.components.loadingOverlay', [])

    .component('loadingOverlay', {
        templateUrl: require('./loading-overlay.html')
    });
