
module.exports = angular.module('cups.components', [
    require('./error-messages/error-messages').name,
    require('./loader/loader').name,
    require('./loading-overlay/loading-overlay').name,
    require('./pagination/pagination').name,
    require('./attachment/attachment').name,
    require('./confirm-modal/confirm-modal').name,
    require('./attachment-set/attachment-set').name,
    require('./form-group/form-group').name
]);
