
export class BaseTable {

    constructor($state) {
        this.$state = $state;
    }

    restoreFilters() {
        let {filters, $state} = this;

        _.each(this.filters, (value, key) => {
            if (!_.isNil($state.params[key])) {
                filters[key].value = $state.params[key];
            }
        });
    }

    serializeFilters() {
        let params = {};

        _.each(this.filters, (filter, key) => {
            if (!_.isNil(filter.value)) {
                params[key] = filter.value;
            }
        });

        return params;
    }

    submit() {
        this.$state.go('.', this.serializeFilters(), {inherit: false});
    }

    clear() {
        _.each(this.filters, (filter) => {
            filter.value = null;
        });
        this.submit();
    }
}
