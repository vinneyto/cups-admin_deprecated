
const templateUrl = require('./error-messages.html');

module.exports = angular.module('cups.components.errorMessages', [])

    .directive('errorMessages', function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                field: '@',
                min: '@',
                max: '@',
                maxlength: '@',
                extensions: '@'
            },
            templateUrl,
            require: '^form',
            link: function (scope, el, attrs, formCtrl) {
                scope.form = formCtrl;

                let formGroup = $(el).parents('.form-group');

                function markFormGroup() {
                    if (formCtrl.$submitted && !isValid()) {
                        formGroup.addClass('has-error');
                    }
                    else formGroup.removeClass('has-error');
                }

                function isValid() {
                    return formCtrl[scope.field] ? formCtrl[scope.field].$valid : true;
                }

                scope.$watch(()=> isValid(), markFormGroup);
                scope.$watch(()=> formCtrl.$submitted, markFormGroup);
            }
        }
    });
