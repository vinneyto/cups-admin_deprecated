
class Controller {
    attachments = [];

    constructor($q, $state, Notification, ConfirmModal) {
        this.$q = $q;
        this.$state = $state;
        this.Notification = Notification;
        this.ConfirmModal = ConfirmModal;
    }

    $onInit() {
        this.updateAttachments();
        this.updateRecordCopy();
    }

    updateRecordCopy() {
        this.recordCopy = this.record.clone();
    }

    updateAttachments() {
        this.attachments = this.record.attachments;
        this.attachmentsCopy = this.attachments.concat();
    }

    hasChanges() {
        return (this.record.fullname !== this.recordCopy.fullname) ||
               (this.record.phone !== this.recordCopy.phone) ||
               (this.record.email !== this.recordCopy.email) ||
               (this.record.description !== this.recordCopy.description) ||
               (!this.isSameAttachments());
    }

    isSameAttachments() {
        return _.isEqual(_.map(this.attachments, 'id'), _.map(this.attachmentsCopy, 'id'));
    }

    save() {
        this.saving = true;

        this.$q.all(_.map(this.attachments, a => a.upload()))
            .then((attachments) => this.record.attachment_ids = _.map(attachments, 'id'))
            .then(() => this.record.DSSave())
            .then((record) => this.record = record)
            .then(() => this.updateAttachments())
            .then(() => this.updateRecordCopy())
            .then(() => this.Notification.success({message: 'Данные сохранены', delay: 3000}))
            .finally(() => this.saving = false);
    }

    remove() {
        this.ConfirmModal.open().then(() => {
            this.removing = true;

            this.record.DSDestroy()
                .then(() => this.$state.go('private.orders'))
                .finally(() => this.removing = false);
        });
    }
}

module.exports = angular.module('cups.components.orderDetails', [
    require('../../scripts/models/order').name,
    require('../../scripts/services/app-state').name
])

    .factory('loadOrderById', function (withLoadingBar, Order, AppState) {
        return function (id) {
            let promise = Order.find(id, {bypassCache: true}).then(record => {
                if (!record.opened) {
                    let updatedRecord;
                    return record.DSUpdate({opened: true}, {partial: true})
                        .then(record => updatedRecord = record)
                        .then(() => AppState.refreshNewOrdersAmount())
                        .then(() => updatedRecord);
                }
                return record;
            });

            return withLoadingBar(promise);
        }
    })

    .component('orderDetails', {
        bindings: {
            record: '<'
        },
        templateUrl: require('./order-details.html'),
        controller: Controller
    });
