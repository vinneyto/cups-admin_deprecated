
const BaseTable = require('../base-table/base-table').BaseTable;

class Controller extends BaseTable {

    filters = {
        fullname: {
            value: null
        },
        phone: {
            value: null
        },
        email: {
            value: null
        },
        opened: {
            options: [
                { name: 'Да', value: true },
                { name: 'Нет', value: false }
            ],
            value: null
        },
        created__gte: {
            value: null
        },
        created__lte: {
            value: null
        }
    };

    constructor($state, Order) {
        super($state);
        this.Order = Order;
        this.restoreFilters();
    }
}

module.exports = angular.module('cups.components.orderList', [
    require('../../scripts/models/order').name
])

    .factory('orderListLoad', function (Order, withLoadingBar) {
        return function (params) {
            let query = {
                _ordering: (params._reverse ? '-' : '') + params._sort,
                _offset: params._offset,
                _limit: params._limit
            };
            if (params.fullname) {
                query.fullname__icontains = params.fullname;
            }
            if (params.phone) {
                query.phone__icontains = params.phone;
            }
            if (params.email) {
                query.email__icontains = params.email;
            }
            if (!_.isNil(params.opened)) {
                query.opened = params.opened;
            }
            if (params.created__gte) {
                query.created__gte = params.created__gte.toISOString();
            }
            if (params.created__lte) {
                query.created__lte = params.created__lte.toISOString();
            }
            return withLoadingBar(Order.findAll(query, {bypassCache: true}));
        }
    })

    .component('orderList', {
        bindings: {
            records: '<'
        },
        templateUrl: require('./order-list.html'),
        controller: Controller
    });
