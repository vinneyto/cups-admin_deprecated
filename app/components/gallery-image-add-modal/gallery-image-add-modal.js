
const templateUrl = require('./gallery-image-add-modal.html');

class Controller {

    constructor($uibModalInstance, $state, GalleryImage, Notification) {
        this.$uibModalInstance = $uibModalInstance;
        this.$state = $state;
        this.GalleryImage = GalleryImage;
        this.Notification = Notification;
        this.name = null;
        this.description = null;
        this.imageAttachment = null;
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }

    save() {
        this.saving = true;
        let { name, description, imageAttachment} = this;

        imageAttachment.upload()
            .then(image_file => this.GalleryImage.createInstance({name, description, image_file}))
            .then(galleryImage => galleryImage.DSCreate())
            .then(galleryImage => this.$state.go('private.gallery-image-details', {id: galleryImage.id}))
            .then(() => this.Notification.success({message: 'Изображение создано', delay: 3000}))
            .then(() => this.cancel())
            .finally(() => this.saving = false);
    }
}

module.exports = angular.module('cups.components.galleryImageAddModal', [
    require('../../scripts/models/gallery-image').name
])

    .factory('GalleryImageAddModal', ($uibModal) => ({
        open: function () {
            return $uibModal.open({
                controller: Controller,
                controllerAs: '$ctrl',
                templateUrl
            }).result;
        }
    }));
