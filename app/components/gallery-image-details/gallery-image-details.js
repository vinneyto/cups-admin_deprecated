
class Controller {

    constructor($state, ConfirmModal, Notification) {
        this.$state = $state;
        this.ConfirmModal = ConfirmModal;
        this.Notification = Notification;
    }

    $onInit() {
        this.updateFileAttachment();
        this.updateRecordCopy();
    }

    updateRecordCopy() {
        this.recordCopy = this.record.clone();
    }

    updateFileAttachment() {
        this.fileAttachment = this.record.image_file;
    }

    hasChanges() {
        return (this.record.name !== this.recordCopy.name) ||
               (this.record.description !== this.recordCopy.description) ||
               (this.fileAttachment && this.fileAttachment.$file);
    }

    save() {
        this.saving = true;

        this.fileAttachment.upload()
            .then((image_file) => this.record.image_file = image_file)
            .then(() => this.record.DSSave())
            .then((record) => this.record = record)
            .then(() => this.updateFileAttachment())
            .then(() => this.updateRecordCopy())
            .then(() => this.Notification.success({message: 'Данные сохранены', delay: 3000}))
            .finally(() => this.saving = false);
    }

    remove() {
        this.ConfirmModal.open().then(() => {
            this.removing = true;

            this.record.DSDestroy()
                .then(() => this.$state.go('private.gallery'))
                .finally(() => this.removing = false);
        });
    }
}

module.exports = angular.module('cups.components.galleryImageDetails', [
    require('../../scripts/models/gallery-image').name
])

    .factory('loadGalleryImageById', function (withLoadingBar, GalleryImage) {
        return function (id) {
            return withLoadingBar(GalleryImage.find(id, {bypassCache: true}));
        }
    })

    .component('galleryImageDetails', {
        bindings: {
            record: '<'
        },
        templateUrl: require('./gallery-image-details.html'),
        controller: Controller
    });
