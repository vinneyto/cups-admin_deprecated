
const templateUrl = require('./confirm-modal.html');

class Controller {

    constructor($uibModalInstance) {
        this.$uibModalInstance = $uibModalInstance;
    }

    ok() {
        this.$uibModalInstance.close();
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }
}

module.exports = angular.module('cups.components.confirmModal', [])

    .factory('ConfirmModal', ($uibModal) => ({
        open: function ({title, message} = {}) {
            return $uibModal.open({
                controller: Controller,
                controllerAs: '$ctrl',
                templateUrl,
                resolve: {title, message}
            }).result;
        }
    }));
