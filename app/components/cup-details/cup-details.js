
class Controller {

    constructor($state, ConfirmModal, Notification) {
        this.$state = $state;
        this.ConfirmModal = ConfirmModal;
        this.Notification = Notification;
    }

    $onInit() {
        this.updateTextureAttachment();
        this.updateRecordCopy();
    }

    updateRecordCopy() {
        this.recordCopy = this.record.clone();
    }

    updateTextureAttachment() {
        this.textureAttachment = this.record.texture;
    }

    hasChanges() {
        return (this.record.name !== this.recordCopy.name) || (this.textureAttachment && this.textureAttachment.$file);
    }

    addCupOnLanding(position) {
        this.addingOnLanding = true;

        this.record.addOnLanding(position)
            .finally(() => this.addingOnLanding = false);
    }

    removeCupFromLanding() {
        this.removingFromLanding = true;

        this.record.removeFromLanding()
            .finally(() => this.removingFromLanding = false);
    }

    save() {
        this.saving = true;

        this.textureAttachment.upload()
            .then((texture) => this.record.texture = texture)
            .then(() => this.record.DSSave())
            .then((record) => this.record = record)
            .then(() => this.updateTextureAttachment())
            .then(() => this.updateRecordCopy())
            .then(() => this.Notification.success({message: 'Данные сохранены', delay: 3000}))
            .finally(() => this.saving = false);
    }

    remove() {
        this.ConfirmModal.open().then(() => {
            this.removing = true;

            this.record.DSDestroy()
                .then(() => this.$state.go('private.cups'))
                .finally(() => this.removing = false);
        });
    }
}

module.exports = angular.module('cups.components.cupDetails', [
    require('../../scripts/models/cup').name
])

    .factory('loadCupById', function (withLoadingBar, Cup) {
        return function (id) {
            return withLoadingBar(Cup.find(id, {bypassCache: true}));
        }
    })

    .component('cupDetails', {
        bindings: {
            record: '<'
        },
        templateUrl: require('./cup-details.html'),
        controller: Controller
    });
