
const BaseTable = require('../base-table/base-table').BaseTable;

class Controller extends BaseTable {

    filters = {
        name: {
            value: null
        },
        created__gte: {
            value: null
        },
        created__lte: {
            value: null
        }
    };

    constructor($state, GalleryImage, GalleryImageAddModal) {
        super($state);
        this.GalleryImage = GalleryImage;
        this.GalleryImageAddModal = GalleryImageAddModal;
        this.restoreFilters();
    }

    addGalleryImage() {
        this.GalleryImageAddModal.open();
    }
}

module.exports = angular.module('cups.components.galleryImageList', [
    require('../../scripts/models/gallery-image').name,
    require('../gallery-image-add-modal/gallery-image-add-modal').name
])

    .factory('galleryImageListLoad', function ($q, GalleryImage, withLoadingBar) {
        return function (params) {
            let query = {
                _ordering: (params._reverse ? '-' : '') + params._sort,
                _offset: params._offset,
                _limit: params._limit
            };
            if (params.name) {
                query.name__icontains = params.name;
            }
            if (params.created__gte) {
                query.created__gte = params.created__gte.toISOString();
            }
            if (params.created__lte) {
                query.created__lte = params.created__lte.toISOString();
            }

            let promise = GalleryImage.findAll(query, {bypassCache: true}).then(records => {
                // preload thumbnails
                let loadThumbnailsPromises = _.map(records, r => $q((resolve, reject) => {
                    let image = new Image();
                    image.src = r.image_file.thumbnail;
                    image.onload = resolve;
                    image.onerror = reject;
                }));

                return $q.all(loadThumbnailsPromises).then(() => records);
            });

            return withLoadingBar(promise);
        }
    })

    .component('galleryImageList', {
        bindings: {
            records: '<'
        },
        templateUrl: require('./gallery-image-list.html'),
        controller: Controller
    });
