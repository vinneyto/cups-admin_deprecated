
class Controller {

    addAttachment(attachment) {
        this.attachments.push(attachment);
        this.validateAttachments();
    }

    setAttachmentAt(index, attachment) {
        if (index < this.attachments.length) {
            this.attachments[index] = attachment;
        }
        this.validateAttachments();
    }

    removeAttachmentAt(index) {
        if (index < this.attachments.length) {
            this.attachments.splice(index, 1);
        }
        this.validateAttachments();
    }

    validateAttachments() {
        // TODO - implement later
    }
}

module.exports = angular.module('cups.components.attachmentSet', [])

    .component('attachmentSet', {
        bindings: {
            attachments: '<ngModel'
        },
        require: {
            ngModel: 'ngModel'
        },
        templateUrl: require('./attachment-set.html'),
        controller: Controller
    });
