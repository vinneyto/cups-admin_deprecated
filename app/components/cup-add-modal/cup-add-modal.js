
const templateUrl = require('./cup-add-modal.html');

class Controller {

    constructor($uibModalInstance, $state, Cup, Notification) {
        this.$uibModalInstance = $uibModalInstance;
        this.Notification = Notification;
        this.$state = $state;
        this.Cup = Cup;
        this.name = null;
        this.textureAttachment = null;
    }

    cancel() {
        this.$uibModalInstance.dismiss();
    }

    save() {
        this.saving = true;
        let { name, textureAttachment} = this;

        textureAttachment.upload()
            .then(texture => this.Cup.createInstance({name, texture}))
            .then(cup => cup.DSCreate())
            .then(cup => this.$state.go('private.cup-details', {id: cup.id}))
            .then(() => this.Notification.success({message: 'Кружка создана', delay: 3000}))
            .then(() => this.cancel())
            .finally(() => this.saving = false);
    }
}

module.exports = angular.module('cups.components.cupAddModal', [
    require('../../scripts/models/cup').name
])

    .factory('CupAddModal', ($uibModal) => ({
        open: function () {
            return $uibModal.open({
                controller: Controller,
                controllerAs: '$ctrl',
                templateUrl
            }).result;
        }
    }));
