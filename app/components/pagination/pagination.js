
class Controller {

    constructor($state) {
        this.$state = $state;
        this.page = ($state.params._offset / $state.params._limit) + 1;
        this.limit = $state.params._limit;
    }

    getStart() {
        return ((this.page - 1) * this.limit) + 1;
    }

    getEnd() {
        return Math.min(this.getStart() + (this.limit - 1), this.total);
    }

    paginate() {
        this.$state.go('.', {_offset: (this.page - 1) * this.$state.params._limit});
    }
}

module.exports = angular.module('cups.components.pagination', [])

    .component('pagination', {
        bindings: {
            total: '='
        },
        templateUrl: require('./pagination.html'),
        controller: Controller
    });
