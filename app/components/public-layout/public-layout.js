
module.exports = angular.module('cups.components.publicLayout', [])

    .component('publicLayout', {
        transclude: true,
        templateUrl: require('./public-layout.html')
    });
