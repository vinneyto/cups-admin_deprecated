
require('./loader.scss');

module.exports = angular.module('cups.components.loader', [])

    .component('loader', {
        templateUrl: require('./loader.html')
    });
