
require('./attachment.scss');

class Controller {

    constructor(CupTexture, GalleryImageFile, Attachment) {
        this.CupTexture = CupTexture;
        this.GalleryImageFile = GalleryImageFile;
        this.Attachment = Attachment;
    }

    getAttachmentModel() {
        switch (this.type) {
            case 'cup-texture':
                return this.CupTexture;
            case 'gallery-image-file':
                return this.GalleryImageFile;
            default:
                return this.Attachment;
        }
    }

    validateFile(file) {
        if (this.accept) {
            let allowedExtensions = this.accept.split(',');
            let fileExtension = file.name.split('.').pop().toLowerCase();

            this.ngModel.$setValidity('extension', _.includes(allowedExtensions, fileExtension));
        }
    }

    selectFile(file) {
        if (!file) { return; }
        let attachment = this.getAttachmentModel().createInstance({$file: file});

        this.ngModel.$setViewValue(attachment);
        this.validateFile(file);

        this.onSelect({$file: file, $attachment: attachment});
    }

    reset() {
        this.ngModel.$setViewValue(null);
        this.ngModel.$setValidity('extension', null);
        this.onReset();
    }

    getTitle() {
        if (this.attachment) {
            return this.attachment.$file ? this.attachment.$file.name : this.attachment.filename;
        } else {
            return 'Выберите или перетащите файл';
        }
    }
}

module.exports = angular.module('cups.components.attachment', [
    require('../../scripts/models/cup-texture').name,
    require('../../scripts/models/gallery-image-file').name,
    require('../../scripts/models/attachment').name
])

    .component('attachment', {
        bindings: {
            attachment: '<ngModel',
            type: '@',
            accept: '@',
            onSelect: '&',
            onReset: '&'
        },
        require: {
            ngModel: 'ngModel'
        },
        templateUrl: require('./attachment.html'),
        controller: Controller
    });
