
module.exports = angular.module('cups.components.formGroup', [])

    .component('formGroup', {
        bindings: {
            for: '@',
            type: '@',
            label: '@',
            required: '<',
            hideSeparator: '<'
        },
        templateUrl: require('./form-group.html'),
        transclude: true
    });
