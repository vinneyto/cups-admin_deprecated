
require('./styles/main.scss');

require('jquery');
require('jquery-ui/ui/widgets/sortable.js');
require('lodash');
require('angular');
require('angular-animate');
require('angular-touch');
require('angular-sanitize');
require('angular-messages');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-ui-notification');
require('angular-ui-sortable');
require('ui-select');
require('js-data');
require('js-data-angular');
require('ng-file-upload');
require('angular-smart-table');
require('angular-loading-bar');

angular.module('cups', [
    'ngAnimate',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ui.router',
    'ui.bootstrap',
    'ui.select',
    'ui.sortable',
    'ui-notification',
    'ngFileUpload',
    'js-data',
    'smart-table',
    'angular-loading-bar',
    require('./scripts/routes').name,
    require('./scripts/filters').name,
    require('./scripts/directives').name,
    require('./scripts/services/auth-storage').name,
    require('./scripts/services/with-loading-bar').name,
    require('./components').name
]);

require('./scripts/initializers');
